#!/bin/bash

# Скрипт для бекапа файлов в S3

# Параметры:
# BACKUP_SCRIPT_STORAGE_NAME - Название бакета
# BACKUP_SCRIPT_STORAGE_FOLDER - Путь до папки для бекапа
# BACKUP_SCRIPT_EXCLUDE - Параметры для исключения
# Пример BACKUP_SCRIPT_EXCLUDE: BACKUP_SCRIPT_EXCLUDE="! -name *.log ! -name *.mpq ! -name *.exe ! -name *.psd"

if [[ -z $BACKUP_SCRIPT_STORAGE_FOLDER ]]; then
  echo "Variable BACKUP_SCRIPT_STORAGE_FOLDER is not exists"
  exit 1
fi;

if [[ -z $BACKUP_SCRIPT_STORAGE_NAME ]]; then
  echo "Variable BACKUP_SCRIPT_STORAGE_NAME is not exists"
  exit 1
fi;

iteration=1
find_exclude=""

if [[ -z $1 ]]; then
  echo "Iteration is not present (use default $iteration). You can restart program as: $0 iteration"
else
  echo "Iteration present - $1"
  iteration=$1
fi;

file_path="/tmp/backup.$iteration.tar.bz2"

if [[ -z $BACKUP_SCRIPT_EXCLUDE ]]; then
  echo "Find exclude is not set. Use default"
else
  echo "Find exclude is present"
  find_exclude=$BACKUP_SCRIPT_EXCLUDE
fi;

echo -e "\nConfig:"
echo "* BACKUP_SCRIPT_EXCLUDE - ($BACKUP_SCRIPT_EXCLUDE)"
echo "* BACKUP_SCRIPT_STORAGE_NAME - ($BACKUP_SCRIPT_STORAGE_NAME)"
echo "* BACKUP_SCRIPT_STORAGE_FOLDER - ($BACKUP_SCRIPT_STORAGE_FOLDER)"
echo "* Use find exclude - ($find_exclude)"
echo "* Use file path - ($file_path)"
echo "* Use iteration - ($iteration)"

echo -e "\n"

if [[ -f $file_path ]]; then
  echo "Remove temp backup"
  rm -f $file_path
fi;

echo "String arhiving file"
find $BACKUP_SCRIPT_STORAGE_FOLDER -type f $find_exclude -print0 | GZIP=-9 tar --warning=no-file-changed -cjvf $file_path -T - > /dev/null

echo "String upload file"
rclone copy -P $file_path backup:$BACKUP_SCRIPT_STORAGE_NAME/
