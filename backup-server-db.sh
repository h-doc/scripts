#!/bin/bash

# Скрипт для бекапа базы данных
# BACKUP_DATABASE_SCRIPT_PATH - Путь до папки где будут храниться бекапами

if [[ -z $BACKUP_DATABASE_SCRIPT_PATH ]]; then
  echo "Variable BACKUP_DATABASE_SCRIPT_PATH is not exists"
  exit 1
fi;

if [[ ! -d $BACKUP_DATABASE_SCRIPT_PATH ]]; then
  echo "Directory for backup is not exists. Create: $BACKUP_DATABASE_SCRIPT_PATH"
  mkdir $BACKUP_DATABASE_SCRIPT_PATH
fi;

echo "Start dump all databases"

FILE_NAME="backup_$(date +"%jday_clc%Hh%Mm_%dd%mm%Yy").gz"
FILE_PATH="$BACKUP_DATABASE_SCRIPT_PATH/$FILE_NAME"

mysqldump -u root -proot --all-databases | gzip -c > "$FILE_PATH"
