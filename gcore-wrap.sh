#!/bin/bash

# Скрипт для запуска бинарика и авто дампа памяти.
# Пример запуска: ./gcore-wrap.sh /opt/bin/server /opt/gcore_dumps

set -e 

# Gcore default path
GCORE_DIRECTORY=/opt/gcore

if [[ -z $1 ]]; then 
	echo "Binary path is empty. Example start: ./$0 binary_path gcore_path"
	exit 1
fi;

if [[ ! -z $2 ]]; then 
	GCORE_DIRECTORY=$2
fi;

if [[ ! -f $1 ]]; then 
	echo "File ($1) does not exists!"
	exit 1
fi;

echo "Starting binary ($1)"
echo "Save path: $GCORE_DIRECTORY"

if [[ ! -d $GCORE_DIRECTORY ]]; then 
	mkdir $GCORE_DIRECTORY;
fi;

while true; do
	current_date="$(date +%s)"
	gcore_save_path="$GCORE_DIRECTORY/$current_date.core"
	binary_save_path="$GCORE_DIRECTORY/$current_date.binary"
	
	echo "Start binary. Mem dump will save to $gcore_save_path"
	cp $1 $binary_save_path
	gdb -ex "set confirm off" -ex "r" -ex "gcore $gcore_save_path" -ex "quit"  $1

	echo "Wait 15 s"
	sleep 15
	
	echo -e "\n\n"
done;
